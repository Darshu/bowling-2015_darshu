note
	description: "Summary description for {GAME_IMPLEMENTATION}."
	author: "Mattia Monga"

class
	GAME_IMPLEMENTATION

inherit
	GAME


create
	make



feature

	score: INTEGER
		do
			Result := points
		end

	roll (pins: INTEGER)
		do

			current_roll := current_roll + 1
			current_frame_rolls:= current_frame_rolls + 1
			pin_count:= pin_count +pins



			points:= points + double_points1*pins


			double_points1:=double_points2
			double_points2:= 1


			if pin_count=10  then
				if pins=10 and current_frame<10 then
					double_points1:= double_points1+1
					double_points2:= double_points2+1
				elseif current_frame_rolls=2 and current_frame<10 then
					double_points1:= double_points1+1
				end
				if current_frame=10 and extra_roll= false then
					extra_roll:=true
				end
			end

			if current_frame= 10 then
				if  (current_frame_rolls= 2 and extra_roll= false) or current_frame_rolls= 3   then
					endgame:= true
				end
			elseif current_frame_rolls= 2 or pin_count=10  then
				current_frame_rolls:=0
				pin_count:=0
				current_frame:= current_frame +1
			end

		end

	ended: BOOLEAN
		do
			Result := endgame
		end

feature {NONE}

	current_roll: INTEGER
	current_frame: INTEGER
	current_frame_rolls:INTEGER
	pin_count: INTEGER
	points: INTEGER
	double_points1: INTEGER
	double_points2: INTEGER
    endgame: BOOLEAN
    extra_roll:BOOLEAN

    make
		do
			double_points1:= 1
			double_points2:= 1
			current_roll:= 0
			current_frame_rolls:= 0
			pin_count:= 0
			endgame:=false
			current_frame:=1
			extra_roll:=false
		end


invariant
	valid_roll: 0 <= current_roll and current_roll <= 21

end
